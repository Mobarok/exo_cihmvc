<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/22/2018
 * Time: 10:42 AM
 */
?>
<link rel="icon" href="<?php echo base_url();?>admin-assets/img/basic/favicon.ico" type="image/x-icon">
<title>EXO | Admin Template</title>
<!-- CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>admin-assets/css/app.css">
<style>
    .loader {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: #F5F8FA;
        z-index: 9998;
        text-align: center;
    }

    .plane-container {
        position: absolute;
        top: 50%;
        left: 50%;
    }
</style>

<?php
    if( !empty($style))
        $this->load->view($style);
?>