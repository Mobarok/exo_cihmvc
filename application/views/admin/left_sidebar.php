<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/22/2018
 * Time: 10:51 AM
 */
?>

        <div class="w-80px mt-3 mb-3 ml-3">
            <img src="<?php echo base_url();?>admin-assets/img/basic/logo.png" alt="">
        </div>
        <div class="relative">
            <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false"
               aria-controls="userSettingsCollapse" class="btn-fab btn-fab-sm fab-right fab-top btn-primary shadow1 ">
                <i class="icon icon-cogs"></i>
            </a>
            <div class="user-panel p-3 light mb-2">
                <div>
                    <div class="float-left image">
                        <img class="user_avatar" src="<?php echo base_url();?>admin-assets/img/dummy/u2.png" alt="User Image">
                    </div>
                    <div class="float-left info">
                        <h6 class="font-weight-light mt-2 mb-1">Alexander Pierce</h6>
                        <a href="#"><i class="icon-circle text-primary blink"></i> Online</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="collapse multi-collapse" id="userSettingsCollapse">
                    <div class="list-group mt-3 shadow">
                        <a href="#" class="list-group-item list-group-item-action ">
                            <i class="mr-2 icon-umbrella text-blue"></i>Profile
                        </a>
                        <a href="#" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-cogs text-yellow"></i>Settings</a>
                        <a href="#" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-security text-purple"></i>Change Password</a>
                        <a href="<?php echo base_url().'login/logout/'.$user_id; ?>" class="list-group-item list-group-item-action"><i
                                    class="mr-2 icon-sign-out text-red"></i>Logout</a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header"><strong>MAIN NAVIGATION</strong></li>
            <li class="treeview"><a href="#">
                    <i class="icon icon-dashboard purple-text s-18"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview"><a href="#">
                    <i class="icon icon icon-cog blue-text s-18"></i>
                    <span>Settings</span>
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>Info</a></li>
                    <li><a href="#"><i class="icon icon-circle-o"></i>Slider</a></li>
                    <li><a href="#"><i class="icon icon-add"></i>Social Navigation</a></li>
                    <li><a href="#"><i class="icon icon-add"></i>Contact</a></li>
                    <li><a href="#"><i class="icon icon-add"></i>Footer</a></li>
                </ul>
            </li>

            <li class="treeview"><a href="#"><i class="icon icon-account_box light-green-text s-18"></i>Users<i
                            class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>All Users</a>
                    </li>
                    <li><a href="#"><i class="icon icon-add"></i>Add User</a>
                    </li>
                    <li><a href="#"><i class="icon icon-user"></i>User Profile </a>
                    </li>
                </ul>
            </li>
            <li class="treeview no-b"><a href="#">
                    <i class="icon icon-package light-green-text s-18"></i>
                    <span>Inbox</span>
                    <span class="badge r-3 badge-success pull-right">20</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>All Messages</a>
                    </li>
                    <li><a href="#"><i class="icon icon-add"></i>Compose</a>
                    </li>
                </ul>
            </li>
            <li class="header light mt-3"><strong>Navigation</strong></li>
            <li class="treeview"><a href="#"><i class="icon icon-list-ul light-green-text s-18"></i>Main Menu<i
                            class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-add"></i>Add Menu </a></li>
                    <li><a href="#"><i class="icon icon-circle"></i>All Menus</a></li>
                </ul>
            </li>

            <li class="treeview"><a href="#"><i class="icon icon-list-ul light-green-text s-18"></i>Footer Menu<i
                            class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-add"></i>Add Menu </a></li>
                    <li><a href="#"><i class="icon icon-circle"></i>All Menus</a></li>
                </ul>
            </li>

            <li class="header light mt-3"><strong>Site Content</strong></li>
            <li class="treeview"><a href="#"><i class="icon icon-cubes teal-text s-18"></i>Services<i
                            class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>All Services</a>
                    </li>
                    <li><a href="#"><i class="icon icon-add"></i>Add Service</a>
                    </li>
                    <li><a href="#"><i class="icon icon-room_service"></i>Highlight Services </a>
                    </li>
                </ul>
            </li>
            <li class="treeview"><a href="#"><i class="icon icon-box2 red-text accent-2 s-18"></i>Working Process<i
                            class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>Add title </a></li>
                    <li><a href="#"><i class="icon icon-add"></i>All Process</a></li>
                </ul>
            </li>

            <li class="treeview"><a href="#"><i class="icon icon-school light-blue-text s-18"></i>Testimonials<i
                            class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>Add title </a></li>
                    <li><a href="#"><i class="icon icon-add"></i>All testimonials</a></li>
                    <li><a href="#"><i class="icon icon-add"></i>Add testimonial</a></li>
                </ul>
            </li>
            <li class="treeview"><a href="#"><i class="icon icon-money light-green-text s-18"></i>Prices/Plans<i
                            class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>Add title </a></li>
                    <li><a href="#"><i class="icon icon-add_box"></i>Add Plan </a></li>
                    <li><a href="#"><i class="icon icon-add"></i>All plans</a></li>
                </ul>
            </li>

        </ul>

