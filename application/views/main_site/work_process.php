<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 6:47 PM
 */
?>

<div class="process-holder container" data-scroll-index="2">
    <div class="row">
        <header class="col-xs-12 header text-center">
            <h4>How <span class="clr">Exo</span> works</h4>
            <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod <br>suspendisse vel, sed quam nulla mauris iaculis.</p>
        </header>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <!-- process list of the page -->
            <ul class="list-unstyled process-list">
                <li class="text-center">
                    <span class="num md-round">1</span>
                    <div class="icon"><img src="<?php echo base_url();?>assets/images/icon04.png" alt="image description" class="img-responsive"></div>
                    <h3>Quickest signup</h3>
                    <p>Lorem ipsum dolor sit amet, et <br>fermentum vestibulum.</p>
                </li>
                <li class="text-center">
                    <span class="num md-round">2</span>
                    <div class="icon"><img src="<?php echo base_url();?>assets/images/icon05.png" alt="image description" class="img-responsive"></div>
                    <h3>Way to work</h3>
                    <p>Lorem ipsum dolor sit amet, et <br>fermentum vestibulum.</p>
                </li>
                <li class="text-center">
                    <span class="num md-round">3</span>
                    <div class="icon"><img src="<?php echo base_url();?>assets/images/icon06.png" alt="image description" class="img-responsive"></div>
                    <h3>Fly to the success</h3>
                    <p>Lorem ipsum dolor sit amet, et <br>fermentum vestibulum.</p>
                </li>
            </ul>
            <!-- process list of the page end -->
        </div>
    </div>
</div>
<!-- process holder of the page end -->

