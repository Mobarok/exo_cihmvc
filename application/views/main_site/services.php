<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 6:41 PM
 */
?>


<div data-scroll-index="1">
    <!-- service holder of the page -->
    <div class="service-holder container">
        <div class="row holder">
            <div class="col-xs-12">
                <!-- service list of the page -->
                <ul class="list-unstyled service-list">
                    <li class="text-center">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/icon01.png" alt="Icon" class="img-responsive"></div>
                        <h2 class="heading">Great for analysis</h2>
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</p>
                    </li>
                    <li class="text-center">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/icon02.png" alt="Icon" class="img-responsive"></div>
                        <h2 class="heading">Great for analysis</h2>
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</p>
                    </li>
                    <li class="text-center">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/icon03.png" alt="Icon" class="img-responsive"></div>
                        <h2 class="heading">Great for analysis</h2>
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</p>
                    </li>
                </ul>
                <!-- service list of the page end -->
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-5">
                <div class="txt-holder">
                    <h3 class="heading2">Seo Service Generator</h3>
                    <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                    <a href="#" class="btn-primary md-round">Learn more <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7">
                <div class="img-holder">
                    <img src="<?php echo base_url();?>assets/images/img02.png" alt="image description" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
    <!-- service holder of the page end -->
    <!-- feature sec of the page -->
    <section class="feature-sec bg-full" style="background-image: url(<?php echo base_url();?>assets/images/img04.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="img-holder">
                        <img src="<?php echo base_url();?>assets/images/img03.png" alt="image description" class="img-responsive">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="txt-holder">
                        <h4 class="heading2">Everything that you need <br>for seo service</h4>
                        <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod suspendisse vel, sed quam nulla mauris iaculis. Erat eget vitae malesuada, tincidunt porta lorem lectus.</p>
                        <a href="#" class="btn-primary md-round">Learn more <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- feature sec of the page end -->
</div>
<!-- process holder of the page -->

