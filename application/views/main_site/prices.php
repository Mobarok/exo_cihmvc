<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 7:10 PM
 */
?>

<section class="price-sec" data-scroll-index="4">
    <div class="container">
        <div class="row">
            <header class="col-xs-12 header text-center">
                <h4>Our pricing &amp; plans</h4>
                <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod <br>suspendisse vel, sed quam nulla mauris iaculis.</p>
            </header>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <!-- price holder of the page -->
                <ul class="list-unstyled price-holder text-center md-round">
                    <li class="price">
                        <strong>$10</strong>
                        <span class="text-uppercase">per month</span>
                    </li>
                    <li class="price-heading text-uppercase">basic</li>
                    <li>Limited dashboard features</li>
                    <li>Unlimited keyword research</li>
                    <li>Advanced analytics tool</li>
                    <li class="btns"><a href="#" class="btn-primary text-uppercase md-round">sign up</a></li>
                </ul>
                <!-- price holder of the page end -->
            </div>
            <div class="col-xs-12 col-sm-4">
                <!-- price holder of the page -->
                <ul class="list-unstyled price-holder text-center md-round active">
                    <li class="price">
                        <strong>$20</strong>
                        <span class="text-uppercase">per month</span>
                    </li>
                    <li class="price-heading text-uppercase">Professional</li>
                    <li>Limited dashboard features</li>
                    <li>Unlimited keyword research</li>
                    <li>Advanced analytics tool</li>
                    <li class="btns"><a href="#" class="btn-primary text-uppercase md-round">sign up</a></li>
                </ul>
                <!-- price holder of the page end -->
            </div>
            <div class="col-xs-12 col-sm-4">
                <!-- price holder of the page -->
                <ul class="list-unstyled price-holder text-center md-round">
                    <li class="price">
                        <strong>$35</strong>
                        <span class="text-uppercase">per month</span>
                    </li>
                    <li class="price-heading text-uppercase">Golden</li>
                    <li>Limited dashboard features</li>
                    <li>Unlimited keyword research</li>
                    <li>Advanced analytics tool</li>
                    <li class="btns"><a href="#" class="btn-primary text-uppercase md-round">sign up</a></li>
                </ul>
                <!-- price holder of the page end -->
            </div>
        </div>
    </div>
</section>

