<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 6:50 PM
 */
?>

<section class="testimonail-sec" data-scroll-index="3">
    <div class="container">
        <div class="row">
            <header class="col-xs-12 header text-center">
                <h4>What our <span class="clr">clients</span> say</h4>
                <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod <br>suspendisse vel, sed quam nulla mauris iaculis.</p>
            </header>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <!-- testimonail slider of the page -->
                <div class="testimonail-slider">
                    <!-- slide of the page -->
                    <blockquote class="slide text-center">
                        <div class="img-holder round"><img src="<?php echo base_url();?>assets/images/img05.jpg" alt="image description" class="img-responsive"></div>
                        <cite>
                            <strong>David Ramon</strong>
                            <a href="#">www.sitename.com</a>
                        </cite>
                        <q>Lorem ipsum dolor sit amet, duis metus amet in purus, vitae donec vestibulum tincidunt massa sit, convallis ipsum pede quisque arcu, nunc distinctio.</q>
                    </blockquote>
                    <!-- slide of the page end -->
                    <!-- slide of the page -->
                    <blockquote class="slide text-center">
                        <div class="img-holder round"><img src="<?php echo base_url();?>assets/images/img06.jpg" alt="image description" class="img-responsive"></div>
                        <cite>
                            <strong>Jennifer Duren</strong>
                            <a href="#">www.sitename.com</a>
                        </cite>
                        <q>Lorem ipsum dolor sit amet, duis metus amet in purus, vitae donec vestibulum tincidunt massa sit, convallis ipsum pede quisque arcu, nunc distinctio.</q>
                    </blockquote>
                    <!-- slide of the page end -->
                    <!-- slide of the page -->
                    <blockquote class="slide text-center">
                        <div class="img-holder round"><img src="<?php echo base_url();?>assets/images/img07.jpg" alt="image description" class="img-responsive"></div>
                        <cite>
                            <strong>Martin Jones</strong>
                            <a href="#">www.sitename.com</a>
                        </cite>
                        <q>Lorem ipsum dolor sit amet, duis metus amet in purus, vitae donec vestibulum tincidunt massa sit, convallis ipsum pede quisque arcu, nunc distinctio.</q>
                    </blockquote>
                    <!-- slide of the page end -->
                    <!-- slide of the page -->
                    <blockquote class="slide text-center">
                        <div class="img-holder round"><img src="<?php echo base_url();?>assets/images/img05.jpg" alt="image description" class="img-responsive"></div>
                        <cite>
                            <strong>David Ramon</strong>
                            <a href="#">www.sitename.com</a>
                        </cite>
                        <q>Lorem ipsum dolor sit amet, duis metus amet in purus, vitae donec vestibulum tincidunt massa sit, convallis ipsum pede quisque arcu, nunc distinctio.</q>
                    </blockquote>
                    <!-- slide of the page end -->
                    <!-- slide of the page -->
                    <blockquote class="slide text-center">
                        <div class="img-holder round"><img src="<?php echo base_url();?>assets/images/img05.jpg" alt="image description" class="img-responsive"></div>
                        <cite>
                            <strong>David Ramon</strong>
                            <a href="#">www.sitename.com</a>
                        </cite>
                        <q>Lorem ipsum dolor sit amet, duis metus amet in purus, vitae donec vestibulum tincidunt massa sit, convallis ipsum pede quisque arcu, nunc distinctio.</q>
                    </blockquote>
                    <!-- slide of the page end -->
                    <!-- slide of the page -->
                    <blockquote class="slide text-center">
                        <div class="img-holder round"><img src="<?php echo base_url();?>assets/images/img06.jpg" alt="image description" class="img-responsive"></div>
                        <cite>
                            <strong>Jennifer Duren</strong>
                            <a href="#">www.sitename.com</a>
                        </cite>
                        <q>Lorem ipsum dolor sit amet, duis metus amet in purus, vitae donec vestibulum tincidunt massa sit, convallis ipsum pede quisque arcu, nunc distinctio.</q>
                    </blockquote>
                    <!-- slide of the page end -->
                </div>
                <!-- testimonail slider of the page end -->
            </div>
        </div>
    </div>
</section>

