<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 7:24 PM
 */
?>

<div class="popup-holder">
    <div id="popup1" class="lightbox">
        <!-- newsletter-block of the page start here -->
        <div class="newsletter-block">
            <h2>Join EXO</h2>
            <p>Join the thousands of people on EXO that get updated on the latest release of free design files and articles.</p>
            <div class="sign-form">
                <form id="contactForm" data-toggle="validator">
                    <fieldset>
                        <div class="form-group">
                            <input type="text" id="name" class="form-control lg-round" placeholder="Your Name" required data-error="NEW ERROR MESSAGE">
                        </div>
                        <div class="form-group">
                            <input type="email" id="email" class="form-control lg-round" placeholder="Your Email" required data-error="NEW ERROR MESSAGE">
                        </div>
                        <div class="form-group">
                            <input type="text" id="subject" class="form-control lg-round" placeholder="Subject" required data-error="NEW ERROR MESSAGE">
                        </div>
                        <div class="form-group">
                            <textarea placeholder="Message" id="message" required data-error="NEW ERROR MESSAGE"></textarea>
                        </div>
                        <div id="msgSubmit" class="form-message hidden"></div>
                        <button type="submit" class="btn-sub lg-round text-center" id="form-submit">Join Now</button>
                    </fieldset>
                </form>
            </div>
            <div class="text-center">
                <span class="txt">* We don’t spam customers. Check our <a href="#">Privacy Policy</a> to learn more.</span>
            </div>
        </div>
        <!-- newsletter-block of the page end here -->
    </div>
</div>
