<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 7:20 PM
 */
?>

<section class="subscriber-sec container" data-scroll-index="5">
    <div class="row">
        <header class="col-xs-12 header text-center">
            <h4>Subscribe to <span class="clr">our newsletter</span></h4>
            <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod <br>suspendisse vel, sed quam nulla mauris iaculis.</p>
        </header>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
                <form action="//htmlbeans.us11.list-manage.com/subscribe/post?u=cb2d5a07fdf0d86c96f260674&amp;id=8e2ec675d3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate newsletter-form md-round" target="_blank" novalidate>
                    <fieldset>
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <!-- <label for="mce-EMAIL">Email Address </label> -->
                                <input type="email" class="form-control md-round" placeholder="Enter your email address">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_cb2d5a07fdf0d86c96f260674_8e2ec675d3" tabindex="-1" value=""></div>
                            <div class="clear"><button class="btn-sub md-round" id="mc-embedded-subscribe" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></div>

                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</section>

