<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 7:31 PM
 */
?>


<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- logo of the page -->
                <div class="logo">
                    <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/images/logo.png" alt="EXO" class="img-responsive"></a>
                </div>
                <!-- logo of the page end -->
                <!-- nav-holder of the page -->
                <div class="nav-holder">
                    <a href="#" class="nav-opener"><i class="fa fa-bars"></i><i class="fa fa-times"></i></a>
                    <!-- nav of the page -->
                    <nav id="nav">
                        <ul class="list-unstyled">
                            <li><a href="#" data-scroll-nav="0">Home</a></li>
                            <li><a href="#" data-scroll-nav="1">About</a></li>
                            <li><a href="#" data-scroll-nav="2">Services</a></li>
                            <li><a href="#" data-scroll-nav="3">Testimonail</a></li>
                            <li><a href="#" data-scroll-nav="4">Price</a></li>
                            <li><a href="#" data-scroll-nav="5">Contact</a></li>
                        </ul>
                    </nav>
                    <!-- nav of the page end -->
                    <ul class="list-unstyled sign-list">
                        <li><a href="#">Log in</a></li>
                        <li><a href="#" class="bg-grey md-round">Sign up</a></li>
                    </ul>
                </div>
                <!-- nav-holder of the page end -->
            </div>
        </div>
    </div>
</header>

