<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 6:37 PM
 */
?>

<!-- main slider of the page -->
<section class="main-slider" data-scroll-index="0">
    <!-- slider of the page -->
    <div class="slide bg-full" style="background-image: url(<?php echo base_url();?>assets/images/img01.jpg);">
        <div class="holder">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>We are providing best services <br class="hidden-xs">for your website</h1>
                        <p>Let us help you to expand your business with the greatest SEO service. <br class="hidden-xs">The complete solution for marketing services</p>
                        <div class="btn-holder">
                            <a href="#" class="btn-primary text-center text-uppercase active md-round">sign up</a>
                            <a href="#" class="btn-primary text-center text-uppercase md-round">Learn more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider of the page end -->
</section>
<!-- main slider of the page end -->

