<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 7:21 PM
 */
?>
<footer id="footer">
    <!-- footer area of the page -->
    <div class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="f-logo">
                        <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/images/f-logo.png" alt="EXO" class="img-responsive"></a>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh eui smod tincidunt ut laoreet dolore magna.</p>
                    <!-- socail network of the page -->
                    <ul class="list-unstyled socail-network">
                        <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" class="tumblr"><i class="fa fa-tumblr"></i></a></li>
                        <li><a href="#" class="yelp"><i class="fa fa-yelp"></i></a></li>
                    </ul>
                    <!-- socail network of the page end -->
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <h3 class="heading">Important Links</h3>
                    <!-- f nav of the page -->
                    <ul class="list-unstyled f-nav">
                        <li><a href="#">Support</a></li>
                        <li><a href="#">Privacy &amp; Policy</a></li>
                        <li><a href="#">Terms &amp; Conditions</a></li>
                        <li><a href="#">VPN Service</a></li>
                        <li><a href="#">Dedicated Server</a></li>
                    </ul>
                    <!-- f nav of the page end -->
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <h3 class="heading">Our Partners</h3>
                    <!-- f nav of the page -->
                    <ul class="list-unstyled f-nav">
                        <li><a href="#">ThemeForest</a></li>
                        <li><a href="#">GraphicRiver</a></li>
                        <li><a href="#">AudioJungle</a></li>
                        <li><a href="#">3DOcean</a></li>
                        <li><a href="#">CodeCanayon</a></li>
                    </ul>
                    <!-- f nav of the page end -->
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <h3 class="heading">Contact with us</h3>
                    <ul class="list-unstyled contact-list">
                        <li><i class="fa fa-phone"></i> <a href="tel:2145212829">+214-5212-829</a></li>
                        <li><i class="fa fa-envelope"></i> <a href="mailto:&#115;&#117;&#112;&#112;&#111;&#114;&#116;&#064;&#101;&#120;&#111;&#046;&#099;&#111;&#109;">&#115;&#117;&#112;&#112;&#111;&#114;&#116;&#064;&#101;&#120;&#111;&#046;&#099;&#111;&#109;</a></li>
                    </ul>
                    <a href="#popup1" class="btn-primary md-round lightbox text-uppercase">Send us a message</a>
                </div>
            </div>
        </div>
    </div>
    <!-- footer area of the page end -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 copyright">
                <p>© Copyright 2017 Exo, All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>

