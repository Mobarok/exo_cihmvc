<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/22/2018
 * Time: 10:12 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller{

    public function __construct()
    {
        parent::__construct();
    }

    public function index( $data = null)
    {
        if($data==null):
            $data['content'] = '';
        endif;

        $this->load->view('index', $data);
    }

}
?>

