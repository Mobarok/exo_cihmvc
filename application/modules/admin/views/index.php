<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/22/2018
 * Time: 10:31 AM
 */
?>

<!DOCTYPE html>
<base href="<?php echo base_url();?>">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php $this->load->view('admin/header');?>
</head>
<body class="light">
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
    <aside class="main-sidebar fixed offcanvas shadow">
        <section class="sidebar">
    <?php


    //Left Sidebar
        $this->load->view('admin/left_sidebar');
    ?>

        </section>
    </aside>

    <div class="page has-sidebar-left">

    <?php

        // Top Navigation
        $this->load->view('admin/top_nav');

        //Load Main Body Content
        $this->load->view($content);
    ?>
    <!--Sidebar End-->
    </div>

    <!-- Right Sidebar -->
    <?php $this->load->view('admin/right_sidebar'); ?>
    <!-- /.right-sidebar -->



    <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
    <div class="control-sidebar-bg shadow white fixed"></div>
</div>

<?php $this->load->view('admin/footer')?>

</body>
</html>


