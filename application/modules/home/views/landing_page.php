<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 3:53 PM
 */
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- set the encoding of your site -->
        <meta charset="utf-8">
        <!-- set the viewport width and initial-scale on mobile devices -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- set the apple mobile web app capable -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <!-- set the HandheldFriendly -->
        <meta name="HandheldFriendly" content="True">
        <!-- set the apple mobile web app status bar style -->
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <!-- set the description -->

        <?php $this->view('main_site/header'); ?>
    </head>
    <body>
        <!-- main container of all the page elements -->
        <div id="wrapper">

            <!-- header of the page -->
            <?php $this->view('main_site/nav'); ?>
            <!-- header of the page end -->


            <!-- main of the page -->
            <main id="main">

                <!-- main slider of the page -->
                <?php $this->view('main_site/slider'); ?>
                <!-- main slider of the page end -->


                <!-- Services Section -->
                <?php $this->view('main_site/services'); ?>
                <!-- End Services Section -->


                <!-- Working Process sec of the page -->
                <?php $this->view('main_site/work_process');?>
                <!-- End Working process sec of the page -->

                <!-- testimonail sec of the page -->
                <?php $this->view('main_site/testimonials'); ?>
                <!-- testimonail sec of the page end -->


                <!-- price sec of the page -->
                <?php $this->view('main_site/prices'); ?>
                <!-- price sec of the page end -->

                <!-- subscriber sec of the page -->

                <?php $this->view('main_site/subscribers'); ?>

                <!-- subscriber sec of the page end -->

            </main>
            <!-- main of the page end -->

            <!-- footer of the page -->
            <?php $this->view('main_site/footer'); ?>
            <!-- footer of the page end -->



            <span id="back-top" class="text-center md-round fa fa-angle-up"></span>
            <!-- loader of the page -->
            <div id="loader" class="loader-holder">
                <div class="block"><img src="<?php echo base_url();?>assets/images/svg/bars.svg" width="60" alt="loader"></div>
            </div>

        </div>

        <!-- main container of all the page elements end -->
        <?php $this->view('main_site/join_us'); ?>


        <?php $this->view('main_site/js_scripts'); ?>

    </body>
</html>
