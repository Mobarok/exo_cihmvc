<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/21/2018
 * Time: 3:34 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller{


    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('landing_page');
    }
}