<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/25/2018
 * Time: 12:56 PM
 */

class Registration_Model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function register( $email, $password)
    {
        $data['email'] = $email;
        $data['password'] = $password;
        $data['status'] = 0;

        return $this->db->insert('users', $data);

    }

    public function check_data($column_name, $data)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($column_name, $data);
        $result = $this->db->get();

        return $result->result_array();
    }

}