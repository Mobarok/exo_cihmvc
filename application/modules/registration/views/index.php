<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/25/2018
 * Time: 12:56 PM
 */
?>

<div class="light b-t">
    <div id="primary" class="content-area"
         data-bg-possition="center"
         data-bg-repeat="false"
         style="background: url('assets/img/icon/icon-circles.png');">
        <main id="main" class="site-main">
            <div class="container">
                <div class="col-xl-8 mx-lg-auto p-t-b-80">
                    <header class="text-center">
                        <h1>Create New Account</h1>
                        <p>Join Our wonderful community and let others help you without a single
                            penny</p>
                        <img class="p-t-b-50" src="assets/img/icon/icon-join.png" alt="">
                    </header>
                    <form method="post" action="registration/register">
                        <div class="row">
<!--                            <div class="col-lg-6">-->
<!--                                <div class="form-group">-->
<!--                                    <input type="text" class="form-control form-control-lg" placeholder="First Name">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-lg-6">-->
<!--                                <div class="form-group">-->
<!--                                    <input type="text" class="form-control form-control-lg" placeholder="Last Name">-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="email" name='email' class="form-control form-control-lg" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control form-control-lg" placeholder="Password">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="password" name="re_password" class="form-control form-control-lg"
                                           placeholder="Confirm Password">
                                </div>
                            </div>
                            <?php

                            $message = $this->session->userdata('message');

                            if(!empty($message)):
                                ?>
                                <div class="col-lg-12">
                                    <div class="alert <?php if ($message['type'] == 'success')  echo 'alert-success'; else echo 'alert-danger'; ?> >"
                                        <span><?=$message['text']?></span>
                                    </div>
                                </div>
                                <?php
                                $this->session->set_userdata('message', '');
                            endif;
                            ?>

                            <div class="col-lg-12">
                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Create Account">
                                <p class="forget-pass">A verification email wil be sent to you</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->
</div>

