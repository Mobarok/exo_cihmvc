<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/25/2018
 * Time: 2:42 PM
 */

class Registration extends MY_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Registration_Model');
    }

    public function index()
    {
        $data['content'] = 'registration/index';
        $this->load->view('admin/default', $data);
    }

    public function register()
    {
        $email =  $this->input->post('email');
        $password =  $this->input->post('password');
        $re_password =  $this->input->post('re_password');

        if( $password != $re_password )
        {
            $message = array(
                'type' => 'danger',
                'text' => 'Password do not match!',
            );
            $this->session->set_userdata('message', $message);

            redirect('registration');
        }

        $password = $this->encrypt_decrypt($password);

        $check_unique_email = $this->check_email_unique();

        if($check_unique_email==1){
            redirect('registration');
        }


        $result = $this->Registration_Model->register($email, $password);

        if($result){
            $message = array(
                'type' => 'success',
                'text' => 'Please wait for Admin confirm',
            );

            $this->session->set_userdata('message', $message);
            redirect('registration');
        }
    }

    public function check_email_unique()
    {
        $email= $this->input->post('email');
        $data= $this->Registration_Model->check_data('email',$email);
        if(count($data)>0)
        {
            $message = array(
                'type' => 'danger',
                'text' => 'Email Already Exist!',
            );
            $this->session->set_userdata('message', $message);
            return 1;
        }

        else
        {
            return 0;
        }

    }


    public function check_phone_unique()
    {
        $phone= $this->input->post('phone');
        $data= $this->Registration_Model->check_data('phone',$phone);
        if(count($data)>0)
        {
            $message = array(
                'type' => 'danger',
                'text' => 'Phone Already Exist!',
            );
            $this->session->set_userdata('message', $message);

            return 1;
        }

        else
        {
            return 0;
        }

    }






}