<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/25/2018
 * Time: 11:03 AM
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Login_Model extends CI_Model{


    /* --------- Status ---------
     *  For status check
     *  Status 1 for approved
     *  Status 0 for Unapproved
    */

    public function __construct()
    {
        parent::__construct();
    }

    public function login_check($email, $password)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->where('status', 1);
        $query = $this->db->get();

        return $query->row();
    }


    public function logout( $id, $username )
    {
        $this->db->select('id, email');
        $this->db->from('users');
        $this->db->where('id', $id);
        $this->db->where('username', $username);
        $query = $this->db->get();

        return $query->row();
    }





}