<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/24/2018
 * Time: 6:16 PM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_Model');
        $this->load->module('admin');
    }

    public function index()
    {
        $data['content'] = 'login/index';
        $data['script'] = 'login/script';

        $this->load->view('admin/default', $data);
    }

    public function login_check()
    {
        $email = $this->input->post('email');
        $password = $this->encrypt_decrypt($this->input->post('password'));

        if( empty($email) || empty($password) )
        {
            $message = array(
                'type' => 'danger',
                'text' => "Invalid Email Or Password !!"
            );

            $this->session->set_userdata('message', $message);
            redirect('/login');
        }else{

            $result = $this->Login_Model->login_check($email, $password);

            if( isset($result))
            {
                $this->session->set_userdata('user_id', $result->id);
                $this->session->set_userdata('user_email', $result->email);
                redirect('/dashboard');

            }else{

                $message = array(
                    'type' => 'danger',
                    'text' => "Invalid Email Or Password !!"
                );

                $this->session->set_userdata('message', $message);
                redirect('/login');
            }
        }
    }


    public function logout( $id='' )
    {
        $email = $this->session->userdata('user_email');
        $result = $this->Login_Model->logout($id, $email);

        if(isset($result)){
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('user_email');

            $message = array(
                'type' => 'success',
                'text' => "You are successfully Logout. "
            );

            $this->session->set_userdata('message', $message);
            redirect('/login');

        }

        redirect('/dashboard');
    }


}