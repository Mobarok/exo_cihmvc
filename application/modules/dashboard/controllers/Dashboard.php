<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/22/2018
 * Time: 4:15 PM
 */


class Dashboard extends MY_Controller {


    protected  $user_id;

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');

        $user_id = $this->user_id = $this->session->userdata('user_id');
        $user_email = $this->session->userdata('user_email');

        if( empty($user_email) || empty($user_id))
            redirect('login');

        $this->load->module('admin');
    }

    public function index()
    {
        $data['user_id'] = $this->user_id;
        $data['content'] = 'dashboard/dashboard_v1';
        $this->admin->index($data);
//        $this->load->view('index', $data);
    }
}